# Můj první git repozitář
Upraveno
Toto je můj první git repozitář.
#Úvod do pythonu

```py
print ("ahoj")
```

## Spuštění
``` sh
python main.py
```


## Proměnné

``` py
message = "hello"
print (message)
def say_hello():
print("ahoj")

say_hello()
```

vypiše 'ahoj'


## Parametry
``` py
def say_hello(text):
    print(text)

say_hello("hello")
say_hello("hi")
```
## Součet
```py
def soucet (x, y):
    return x + y
z = soucet(10, 20)
print(z)
```
## Podmínky
```py
def soucet (x, y):
    if x == 0:
        return 1000000
    return x + y
z = soucet(0, 20)
print(z)
```
## Seznam
```py
jmena = ['pepa', 'ferda', 'jarda',]

for jmeno in jmena:
    print(jmeno)

print(jmena[1])
```
## Faktorial
```py
def factorial(x):
    if x == 1 or x == 0:
        return 1
    return x * factorial(x-1)
y = factorial(4)
print (y)
```
## Cyklus
for i in range(x):
print(i)

##Kdyz
```py
cisla = [6, 5, 2, 1, 3, 4, 7, 8, ]

serazeno = False 
while not serazeno:
    serazeno = True
    for i in range(len(cisla)-1):
        if cisla[i] > cisla[i+1]:
            x = cisla[i]
            cisla[i] = cisla[i+1]
            cisla[i+1] = x
            serazeno = False
for x in cisla:
    print(x)
```